"use strict";

var canvas;
var context;
var canvasHeightMap;
var contextHeightMap;
var map;
var seed = 1;
var isHeightMap = false;
var clearMap = false;

function init()
{
	canvas = document.getElementById("myCanvas");
	context = canvas.getContext("2d");
	canvasHeightMap = document.createElement("canvas");
	canvasHeightMap.style.backgroundColor = "black";
	canvasHeightMap.width = canvas.width;
	canvasHeightMap.height = canvas.height;
	contextHeightMap = canvasHeightMap.getContext("2d");
	
	initBrush();
	start();
}

function start()
{
	if(clearMap)
	{
		blankMap();
	}
	else
	{
		createMap(false);
	}
	//calcShoreDistance();
	calcShoreDistance3();
	calcHeights();
	calcColors(true);
	drawMap(true);
	init3d();
	//getOutline();
}

function blankMap()
{
	map = {
		step: 1,
		data: []
	};
	var step = 1;
	for (var x = 0, xIndex = 0; x < canvas.width; x += step, xIndex++) 
	{
		map.data.push([]);
		
		for (var y = 0, yIndex = 0; y < canvas.height; y += step, yIndex++) 
		{
			map.data[xIndex][yIndex] = {
			 	noise: 0,
			 	searched: false,
			 	shoreDistance: -1
			 };
		}
	}
}

function createMap(outline)
{
	//var scale = 0.0057;
	var scale = 4.56 / canvas.width;
	var scale2 = 5882;
	//var scale3 = 28.23;
	var scale3 = 22584 / canvas.width;
	var scale4 = 7972;
	var offset = 66;
	var high;
	var low;
	var inverted = true;
	var step = 1;
	
	//noise.seed(seed);
	noise.seed(seed);
	
	var centerX = (canvas.width / 2);
	var centerY = (canvas.height / 2);
	
	low = 99999999;
	high = -99999999;
	map = {
		step: step,
		data: []
	};
	
	for (var x = 0, xIndex = 0; x < canvas.width; x += step, xIndex++) 
	{
		map.data.push([]);
		
		for (var y = 0, yIndex = 0; y < canvas.height; y += step, yIndex++) 
		{
			var dx = x - centerX;
			var dy = y - centerY;
			var d = Math.sqrt(dx * dx + dy * dy);
			
			var value = (((noise.perlin2(x * scale, y * scale) * scale2 - (d * scale3))) + offset) / scale4;
			
			if(value < low)
				low = value;
			if(value > high)
				high = value;
			
			 var height = (255 - Math.abs(value) * 256);
			 	
			 height = Math.min(height, 255);
			 height = Math.max(height, 0);
			 
			 if(outline && height > 0)
			 	height = 255;
			 
			 map.data[xIndex][yIndex] = {
			 	noise: height,
			 	searched: false,
			 	shoreDistance: -1
			 };
		}
	}

	//alert("hi: " + high + " low: " + low);
}

function calcShoreDistance3()
{
	var queue = [];
	var nextQueue = [];
	queue.push({x: 0, y: 0});
	
	while(queue.length > 0)
	{
		var item = queue.shift();
		if(item.x < map.data.length && item.y < map.data[0].length && item.x >= 0 && item.y >= 0)
		{
			var grid = map.data[item.x][item.y];
			if(!grid.searched)
			{
				grid.searched = true;
				if(grid.noise > 0)
				{
					nextQueue.push({
						x: item.x, 
						y: item.y
					});
				}
				else
				{
					queue.push({
						x: item.x + 1,
						y: item.y
					});
					queue.push({
						x: item.x - 1,
						y: item.y
					});
					queue.push({
						x: item.x,
						y: item.y + 1
					});
					queue.push({
						x: item.x,
						y: item.y - 1
					});
					
					grid.shoreDistance = 0;
				}
			}
		}
	}
	
	queue = nextQueue;
	nextQueue = [];
	var shoreDistance = 1;
	while(queue.length > 0)
	{
		item = queue.shift();
		if(item.x < map.data.length && item.y < map.data[0].length && item.x >= 0 && item.y >= 0)
		{
			var grid = map.data[item.x][item.y];
			if(grid.shoreDistance === -1)
			{
				grid.shoreDistance = shoreDistance;
				nextQueue.push({
					x: item.x + 1,
					y: item.y
				});
				nextQueue.push({
					x: item.x - 1,
					y: item.y
				});
				nextQueue.push({
					x: item.x,
					y: item.y + 1
				});
				nextQueue.push({
					x: item.x,
					y: item.y - 1
				});
			}
		}
		
		if(queue.length === 0)
		{
			queue = nextQueue;
			nextQueue = [];
			shoreDistance++;
		}
	}
}


function calcShoreDistance2()
{
	var search = [];
	for(var x = 0; x < map.data.length; x++)
	{
		for(var y = 0; y < map.data.length; y++)
		{
			var shoreDistance = getShoreDistance2(x, y);
			if(shoreDistance === 1)
			{
				search.push({x: x, y: y});
				map.data[x][y].shoreDistance = shoreDistance;
			}
			else
			{
				//map.data[x][y].shoreDistance = -1;
			}
		}
	}
	
	while(search.length > 0)
	{
		var i = Math.floor(Math.random() * search.length);
		
		var shoreDistance2 = getShoreDistance2(search[i].x, search[i].y);
		
		if(shoreDistance !== -1)
		{
			var shoreDistance = getDistanceToShore(search[i].x, search[i].y);
			
			if(shoreDistance > shoreDistance2)
				shoreDistance = shoreDistance2;
				
			map.data[search[i].x][search[i].y].shoreDistance = shoreDistance;
			search.splice(i, 1);
			break;
		}
	}
}

function getShoreDistance2(x, y)
{
	var shores = [];

	if(map.data[x][y].noise === 0)
		return 0;
	
	var count = 0;
	if(x + 1 < map.data.length)
	{
		count++;
		if(map.data[x + 1][y].shoreDistance !== -1)
		{
			shores.push(map.data[x + 1][y].shoreDistance + 1);
		}
	}
		
	if(x - 1 >= 0)
	{
		count++;
		if(map.data[x - 1][y].shoreDistance !== -1)
		{
			shores.push(map.data[x - 1][y].shoreDistance + 1);
		}
	}
		
	if(y + 1 < map.data[0].length) 
	{ 
		count++;
		if(map.data[x][y + 1].shoreDistance !== -1)
		{
			shores.push(map.data[x][y + 1].shoreDistance + 1);
		}
	}
		
	if(y - 1 >= 0)
	{
		count++;
		if(map.data[x][y - 1].shoreDistance !== -1)
		{
			shores.push(map.data[x][y - 1].shoreDistance + 1);
		}
	}
		
	if(shores.length > 0)
	{
		var minDistance = 999999999;
		for(var i = 0; i < shores.length; i++)
		{
			if(shores[i] < minDistance)
				minDistance = shores[i];
		}
		
		return minDistance;
	}
	
	return -1;
}

function calcShoreDistance()
{
	for(var x = 0; x < map.data.length; x++)
	{
		for(var y = 0; y < map.data.length; y++)
		{
			map.data[x][y].shoreDistance = -1;
			map.data[x][y].shoreDistance = getDistanceToShore(x, y);
			clearSearched();
		}
	}
	
	document.getElementById("msg").innerHTML = maxStackSize;
}

function clearSearched()
{
	for(var x = 0; x < map.data.length; x++)
	{
		for(var y = 0; y < map.data.length; y++)
		{
			map.data[x][y].searched = false;
		}
	}
}

function clearShoreDistance()
{
	for(var x = 0; x < map.data.length; x++)
	{
		for(var y = 0; y < map.data.length; y++)
		{
			map.data[x][y].shoreDistance = -1;
		}
	}
}

var maxStackSize = 0;

function getDistanceToShore(x, y)
{
	var search = [];
	var shores = [];
	var distance = 0;
	search.push({x: x, y: y, shoreDistance: distance});
	
	while(search.length > 0)
	{
		if(search.length > maxStackSize)
			maxStackSize = search.length;
			
		var item = search.pop();
		var grid = map.data[item.x][item.y];
		if(grid == undefined)
			console.log("problem");
	
		//Found the ocean
		if(grid.noise === 0 || grid.shoreDistance > -1)
		{
			if(grid.noise === 0)
				grid.shoreDistance = 0;
			else
			{
				item.shoreDistance += grid.shoreDistance;
			}
			
			if(isNaN(distance))
				console.log("problem");
				
			//console.log(item.shoreDistance);
			shores.push(item.shoreDistance);
		}
		else
		{
			if(item.x + 1 < map.data.length && !map.data[item.x + 1][item.y].searched)
			{
				search.push({
					x: item.x + 1,
					y: item.y,
					shoreDistance: item.shoreDistance + 1
				});
			}
			if(item.x - 1 >= 0 && !map.data[item.x - 1][item.y].searched)
			{
				search.push({
					x: item.x - 1,
					y: item.y,
					shoreDistance: item.shoreDistance + 1
				});
			}
			if(item.y + 1 < map.data[0].length && !map.data[item.x][item.y + 1].searched)
			{
				search.push({
					x: item.x,
					y: item.y + 1,
					shoreDistance: item.shoreDistance + 1
				});
			}
			if(item.y - 1 >= 0 && !map.data[item.x][item.y - 1].searched)
			{
				search.push({
					x: item.x,
					y: item.y - 1,
					shoreDistance: item.shoreDistance + 1
				});
			}
			
			map.data[item.x][item.y].searched = true;
		}
	}
	
	var minDistance = 999999999;
	for(var i = 0; i < shores.length; i++)
	{
		if(shores[i] < minDistance)
			minDistance = shores[i];
	}
	
	return minDistance;
}

function drawMap(completeMap, xStart, yStart, width, height)
{
	if(map.step === 1)
	{
		drawPixels(completeMap, xStart, yStart, width, height);
	}
	else
	{
		drawRects();
	}
}

function calcColors(completeMap, xStart, yStart, width, height)
{
	if(completeMap)
	{
		xStart = 0;
		yStart = 0;
		width = canvas.width;
		height = canvas.height;
	}
	
	var setColorMap = function (pixelHeight) {
		var pct = pixelHeight / map.maxHeight;
		var red = 0;
		var green = 0;
		var blue = 0;
		
		if(pixelHeight === 0)
		{
			//map.data[x][y].color = "blue";
			blue = 255;
		}
		else if(pixelHeight < 3)
		{
			red = 190 + 250 * pct;
			green = 190 + 250 * pct;
		}
		else if(pct < 0.7)
		{
			green = 100 + 220 * pct;
		}
		else if(pct < 0.75)
		{
			red = -300 + 600 * pct;
			green = -325 + 600 * pct;
			blue = -425 + 600 * pct;
		}
		else 
		{
			red = -325 + 600 * pct;
			green = -325 + 600 * pct;
			blue = -325 + 600 * pct;
		}
		
		var colorNoise = Math.random() * 12 - 6;
		
		return {
			red: Math.floor(red + colorNoise), 
			green: Math.floor(green + colorNoise),
			blue: Math.floor(blue + colorNoise)
		};
	};
	var setHeightMap = function (pixelHeight) {
		return {
			red: pixelHeight, 
			green: pixelHeight,
			blue: pixelHeight
		};
	};
	
	for(var x = xStart; x - xStart < width; x++)
	{
		for(var y = yStart; y - yStart < height; y++)
		{
			var pixelHeight = map.data[x][y].height;
			var colors = setColorMap(pixelHeight);
			map.data[x][y].color = "rgb(" + 
					colors.red + ", " + 
					colors.green + ", " + 
					colors.blue + ")";
			colors = setColorMap(pixelHeight);
			map.data[x][y].color2 = {
				red: colors.red, 
				blue: colors.blue, 
				green : colors.green
			};
			map.data[x][y].heightMapColor = setHeightMap(pixelHeight);
		}
	}
}

function calcHeights()
{
	map.maxHeight = map.data.length / 4;
	for(var x = 0; x < map.data.length; x++)
	{
		for(var y = 0; y < map.data[0].length; y++)
		{
			var noise = map.data[x][y].noise;
			var shoreDistance = map.data[x][y].shoreDistance;
			//var height = (shoreDistance + noise) / 5;
			var height = shoreDistance;
			if(height > map.maxHeight)
			{
				map.maxHeight = height;
			}
			map.data[x][y].height = height;
		}
	}
}

function calcFromHeightMap(context, completeMap, xStart, yStart, width, height)
{
	if(completeMap)
	{
		xStart = 0;
		yStart = 0;
		width = canvas.width;
		height = canvas.height;
		//map.maxHeight = 10;
	}
	
	var imageData = context.getImageData(xStart, yStart, width, height);
	var data = imageData.data;
	for(var x = 0; x < width; x++)
	{
		for(var y = 0; y < height; y++)
		{
			var pixelHeight = data[((width * y) + x) * 4];
			map.data[x + xStart][y + yStart].height = pixelHeight;
			// if(pixelHeight > map.maxHeight)
			// {
			// 	map.maxHeight = pixelHeight;
			// }
		}
	}
}

function updateArea(x, y, width, height)
{
	calcFromHeightMap(contextHeightMap, false, x, y, width, height);
	calcColors(false, x, y, width, height);
	drawMap(false, x, y, width, height);
}

function drawRects()
{
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.strokeStyle = "white";
	context.font = "bold " + Math.floor(map.step / 2) + "px Arial";
	
	for(var x = 0; x < map.data.length; x++)
	{
		for(var y = 0; y < map.data.length; y++)
		{
			var noise = map.data[x][y].noise / 30;
			var shoreDistance = map.data[x][y].shoreDistance;
			var height = map.data[x][y].height;
		
			context.fillStyle = map.data[x][y].color;
			context.fillRect(x * map.step, y * map.step, map.step, map.step);
			
			if(height > 0 && map.step > 15)
			{
				context.strokeRect(x * map.step, y * map.step, map.step, map.step);
				
				context.fillStyle = "#FF0099";
				var str = Math.floor(height);
				context.fillText(str, x * map.step, y * map.step + map.step / 2, map.step);
			}
		}
	}
}

function drawPixels(completeMap, xStart, yStart, width, height)
{
	if(completeMap)
	{
		xStart = 0;
		yStart = 0;
		width = canvas.width;
		height = canvas.height;
	}
	
	var imageData = context.getImageData(xStart, yStart, width, height);
	var data = imageData.data;
	
	var imageDataHeightMap = contextHeightMap.getImageData(xStart, yStart, width, height);
	var dataHeightMap = imageDataHeightMap.data;
	
	var getColorMap = function (x, y) {
	/*	return {
			red: map.data[x][y].noise,
			green: map.data[x][y].noise,
			blue: map.data[x][y].noise
		};*/
		return map.data[x][y].color2;
	};
	var getHeightMap  = function (x, y) {
		return map.data[x][y].heightMapColor;
	};
	
	var getColor = getColorMap;	
	if(isHeightMap)
	{
		getColor = getHeightMap;
	}
	for(var x = 0; x < width; x++)
	{
		for(var y = 0; y < height; y++)
		{
			var color = getColor(x + xStart, y + yStart);
			var color2 = getHeightMap(x + xStart, y + yStart);
			var index = ((width * y) + x) * 4;
			
			data[index] = color.red;
			data[index + 1] = color.green;
			data[index + 2] = color.blue;
			data[index + 3] = 255;
			
			dataHeightMap[index] = color2.red;
			dataHeightMap[index + 1] = color2.green;
			dataHeightMap[index + 2] = color2.blue;
			dataHeightMap[index + 3] = 255;
		}
	}
	context.putImageData(imageData, xStart, yStart);
	contextHeightMap.putImageData(imageDataHeightMap, xStart, yStart);
}
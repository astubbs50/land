var mouseX, mouseY, lastMouseX, lastMouseY, mouseDown, mouseDownRight;
var isRotating = false;
var isDirty = false;

function KeyDown(e)
{	
	SetAction(e.keyCode, true);
}

function KeyUp(e)
{
	//document.getElementById("stats").innerHTML = e.keyCode;
	SetAction(e.keyCode, false);
}

function SetAction(keyCode, bOn)
{
	switch(keyCode)
	{
		//Q
		case 81:
			movement.raise = bOn;
			break;
		case 69:
			movement.lower = bOn;
			break;
		//Left
		case 65:
		case 37:
		case 100:
			movement.left = bOn;
			break;
		//Right
		case 68:
		case 39:
		case 102:
			movement.right = bOn;
			break;
		//Up
		case 87:
		case 38:
		case 104:
			movement.up = bOn;
			break;			
		case 40:
		case 83:
			movement.back = bOn;
			break;			
		//Space
		case 32:			
			//player.fire1 = bOn;
			break;
		//Enter
		case 13:			
			//player.fire2 = bOn;
			break;
		//R
		case 82:
			camera.yaw -= 0.02;
			camera.Update(0, 0, 0);
			break;
		//T
		case 84:
			camera.yaw += 0.02;
			camera.Update(0, 0, 0);
			break;
		//F
		case 70:
			camera.pitch -= 0.02;
			camera.Update(0, 0, 0);
			break;
		//G
		case 71:
			camera.pitch += 0.02;
			camera.Update(0, 0, 0);
			break;
	}
}

function GetMouseCoords(e)
{
	if(e.offsetX) 
	{ 
		mouseX = e.offsetX; 
		mouseY = e.offsetY; 
	} 
	else if(e.layerX) 
	{ 
		mouseX = e.layerX; 
		mouseY = e.layerY; 
	}
	
	mouseX = mouseX * (canvas.width / canvas.offsetWidth);
	mouseY = mouseY * (canvas.height / canvas.offsetHeight);
	//document.getElementById("msg2").innerHTML = "x: " + mouseX.toFixed(0) + " y: " + mouseY.toFixed(0);
}

function MouseMove(e)
{
	GetMouseCoords(e);
	
	if(mouseDown && mouseX > 0 && mouseX < canvas.width && mouseY > 0 && mouseY < canvas.height)
	{
		//if(mouseX != lastMouseX && mouseY != lastMouseY)
		//{
			if(isHeightMap)
			{
				DrawOnMap(context);
			}
			else
			{
				DrawOnMap(contextHeightMap);
			}
		//}
	}
}

var lastDrawX = -1;
var lastDrawY = -1;
var lastDrawTime = -1;
var drawDelay = 100;

function DrawOnMap(context)
{
	if(lastDrawX !== mouseX && lastDrawY !== mouseY && t - lastDrawTime > drawDelay)
	{
		context.globalAlpha = 0.05;
		var x = mouseX - brushCanvas.width / 2;
		var y = mouseY - brushCanvas.height / 2;
		
		context.drawImage(brushCanvas, x, y);
		context.globalAlpha = 1;
		lastDrawTime = t;
		isDirty = true;
		
		if(!isHeightMap)
		{
			updateArea(Math.round(x), Math.round(y), brushCanvas.width, brushCanvas.height);
		}
	}
}

function MouseMove2(e)
{
	if(isRotating)
	{
		GetMouseCoords(e);
		
		if(mouseDownRight)
		{
			if(mouseX != lastMouseX || mouseY != lastMouseY)
			{			
				//noMovementCount = 0;
				var xMove = mouseX - lastMouseX;
				var yMove = mouseY - lastMouseY;
				
				if(xMove > 0)
				{
					camera.yaw -= 0.02;
				}
				else if(xMove < 0)
				{
					camera.yaw += 0.02;
				}
				
				if(yMove > 0)
				{
					camera.pitch -= 0.02;
					if(camera.pitch < -1)
					{
						camera.pitch = -1;
					}
				}
				else if(yMove < 0)
				{
					camera.pitch += 0.02;
					if(camera.pitch > 1)
					{
						camera.pitch = 1;
					}
				}
				//document.getElementById("msg2").innerHTML = " pitch: " + camera.pitch.toFixed(2) + " yaw: " + camera.yaw.toFixed(2);
				
				camera.Update(0, 0, 0);
				
				//document.getElementById("msg2").innerHTML = "yaw: " + camera.yaw + " pitch: " + camera.pitch;
			}
		}
		
		lastMouseX = mouseX;
		lastMouseY = mouseY;
	}
}

function DetectLeftButton(e) 
{
    if ('which' in e) 
    {
        return e.which === 1;
    } 
    else if ('buttons' in e) 
    {
        return e.buttons === 1;
    } 
    else 
    {
        return e.button === 1;
    }
}

function MouseDown(e)
{
	if(DetectLeftButton(e))
	{
		mouseDown = true;
		GetMouseCoords(e);
		if(e.target.id === "myCanvas")
		{
			if(isHeightMap)
				DrawOnMap(context);
			else
				DrawOnMap(contextHeightMap);
		}
	}
	else
	{
		if(e.target.id === "myCanvas2")
		{
			mouseDownRight = true;
			isRotating = true;
		}
	}
}

function MouseUp(e)
{
	mouseDown = false;
	mouseDownRight = false;
	if(isDirty)
	{
		if(isHeightMap)
		{
			calcFromHeightMap(context, true);
		}
		else
		{
			calcFromHeightMap(contextHeightMap, true);
		}
		calcColors(true);
		
		if(!isHeightMap)
			drawMap(true);
			
		setupScene();
	}
	
	isDirty = false;
}

function MouseWheel(event)
{
	var delta = 0;

	if (!event)
		event = window.event;

	if (event.wheelDelta) 
	{ 
		delta = event.wheelDelta/120;
	} 
	else if (event.detail) 
	{ 
		delta = -event.detail/3;
	}
 
	//camera.Update(0, 0, -delta, false);
  	camera.position[1] += delta / 10;
	camera.Update(0,0,0);
	if (event.preventDefault)
		event.preventDefault();
	
	event.returnValue = false;
}


function UpdateTextSize()
{
	document.getElementById("txtSize").value = document.getElementById("rngSize").value;
}

function UpdateRangeSize()
{
	document.getElementById("rngSize").value = document.getElementById("txtSize").value;
}

function UpdateTextSeed()
{
	document.getElementById("txtSeed").value = document.getElementById("rngSeed").value;
}

function UpdateRangeSeed()
{
	document.getElementById("rngSeed").value = document.getElementById("txtSeed").value;
}

function UpdateSettings()
{
	seed = Number(document.getElementById("txtSeed").value);
	var lastSize = canvas.width;
	var newSize = Number(document.getElementById("txtSize").value);
	
	canvas.width = newSize;
	canvas.height = newSize;
	canvas2.width = newSize;
	canvas2.height = newSize;
	canvasHeightMap.width = newSize;
	canvasHeightMap.height = newSize;
	isHeightMap = document.getElementById("chkHeightMap").checked;
	clearMap = document.getElementById("chkClearMap").checked;
	
	CloseSettings();
	start();
	initBrush();
}

function CancelSettings()
{
	document.getElementById("txtSeed").value = seed;
	document.getElementById("txtSize").value = canvas.width;
	document.getElementById("rngSeed").value = seed;
	document.getElementById("rngSize").value = canvas.width;
	document.getElementById("chkHeightMap").checked = isHeightMap;
	CloseSettings();
}

function CloseSettings()
{
	document.getElementById("divSettings").style.display = "none";
}

function OpenSettings()
{
	var div = document.getElementById("divSettings");
	div.style.display = "inline-block";
	//div.left = 
}
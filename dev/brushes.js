var brushCanvas;
var brushContext;
function initBrush(canvas, settings)
{
    /* global canvas */
    brushCanvas = canvas;
    brushCanvas.width = canvas.width * settings.size;
    brushCanvas.height = canvas.height * settings.size;
    
    brushContext = brushCanvas.getContext("2d");
    
    brushContext.clearRect(0, 0, brushCanvas.width, brushCanvas.height);
    
    brushContext.fillStyle = "rgba(255,255,255,0.05)";
    var x = brushCanvas.width / 2;
    var y = brushCanvas.height / 2;
    var step = brushCanvas.width * 0.25;
    if(step < 1)
        step = 1;
    for(var r = 0; r < brushCanvas.width / 2; r += step)
    {
        brushContext.arc(x, y, r, 0, Math.PI * 2, false);
        brushContext.fill();
    }
    //brushContext.fillRect(10, 10, brushCanvas.width, brushCanvas.height);
}

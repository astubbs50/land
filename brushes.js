var brushCanvas;
var brushContext;
function initBrush()
{
    brushCanvas = document.getElementById("myCanvasBrush");
    brushContext = brushCanvas.getContext("2d");
    brushContext.width = brushCanvas.width;
    brushContext.height = brushCanvas.height;
    brushContext.clearRect(0, 0, brushCanvas.width, brushCanvas.height);
    
    brushContext.fillStyle = "rgba(255,255,255,0.05)";
    var x = brushCanvas.width / 2;
    var y = brushCanvas.height / 2;
    for(var r = 0; r < brushCanvas.width / 2; r++)
    {
        brushContext.arc(x, y, r, 0, Math.PI * 2, false);
        brushContext.fill();
    }
    //brushContext.fillRect(10, 10, brushCanvas.width, brushCanvas.height);
}
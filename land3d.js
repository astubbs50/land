"use strict";

//Source code for the vertex shader
var vertexShaderStrColors = "" +
	"attribute vec3 aVertexPosition;" +
	"attribute vec4 aVertexColor;" + 
	"uniform mat4 uMVMatrix;" +
	"uniform mat4 uPMatrix;" +
	"varying vec4 vColor;" + 
	"void main(void) {" + 
	    "gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);" +
	    "vColor = aVertexColor;" +
	"}";

//Source code for the fragment shader
var fragmentShaderStrColors = "" +
	"precision mediump float;" +
	"varying vec4 vColor;" + 
	"void main(void) {" +
	    "gl_FragColor = vColor;"+
	"}";

var canvas2;
var webgl;
var mvMatrix = mat4.create();
var pMatrix = mat4.create();
var obj3Ds = [];
var camera;
var border = 0.001;
var maxId = 0;
var movement = {
	left: false,
	right: false,
	up: false,
	back: false
};
var lt = 0;
var t;
var frames = 0;
var running = false;

function init3d()
{
    initGraphics();
    
    setupScene();
    draw3d(webgl);
    
    if(!running)
    {
    	running = true;
    	run();
    }
}

function initGraphics()
{
	canvas2 = document.getElementById("myCanvas2");
	//canvas2.width = canvas2.offsetWidth;
	//canvas2.height = canvas2.offsetHeight;
    
	webgl = initWebGL(canvas2);
	camera = new Camera(canvas2);
	camera.SetFreeMode();
	camera.position = [canvas.width / 2, canvas.width / 4, canvas.height];
	camera.pitch = -0.7;
	camera.yaw = 0;
	camera.Update(0, 0, 0);
	
	initShaders(webgl, fragmentShaderStrColors, vertexShaderStrColors, false);
}

function initWebGL(canvas) 
{
	var webgl;
	
	var contextNames = ["webgl","experimental-webgl", "webkit-3d", "moz-webgl"];	
	var contextName;
	for(var i = 0; i < contextNames.length && !webgl; i++)
	{
		contextName = contextNames[i];
		try 
		{
			webgl = canvas.getContext(contextName);
			webgl.viewportWidth = canvas.width;
			webgl.viewportHeight = canvas.height;
			webgl.enable(webgl.DEPTH_TEST);
		} 
		catch (e) 
		{

		}
	}

	if (!webgl) 
	{
	    alert("Could not initialise WebGL");
	}
	else
	{
		//alert("Initialized WebGL with " + contextName);
		webgl.clearColor(0.9, 0.9, 0.9, 1);
		webgl.enable(webgl.DEPTH_TEST);
        webgl.clear(webgl.COLOR_BUFFER_BIT | webgl.DEPTH_BUFFER_BIT);
	//	webgl.clear(webgl.COLOR_BUFFER_BIT);
		webgl.viewport(0, 0, webgl.viewportWidth, webgl.viewportHeight);
		
		return webgl;
	}
}

function initShaders(webgl, fragmentShaderStr, vertexShaderStr, isTexture)
{
	//Create & compile the fragment shader
	var fragmentShader = webgl.createShader(webgl.FRAGMENT_SHADER);
	var shaderProgram;
	
	webgl.shaderSource(fragmentShader, fragmentShaderStr);
    webgl.compileShader(fragmentShader);

	if (!webgl.getShaderParameter(fragmentShader, webgl.COMPILE_STATUS)) 
	{
		alert("fragmentShader: " + webgl.getShaderInfoLog(fragmentShader));
		return null;
	}

	//Create and compile the vertex shader
	var vertexShader = webgl.createShader(webgl.VERTEX_SHADER);
	webgl.shaderSource(vertexShader, vertexShaderStr);
	webgl.compileShader(vertexShader);

	if (!webgl.getShaderParameter(vertexShader, webgl.COMPILE_STATUS)) 
	{
		alert("vertexShader: " + webgl.getShaderInfoLog(vertexShader));
		return null;
	}

	//Create the shader program
	shaderProgram = webgl.createProgram();
	webgl.attachShader(shaderProgram, vertexShader);
	webgl.attachShader(shaderProgram, fragmentShader);
	webgl.linkProgram(shaderProgram);

	//Assign the active program to this shaderProgram
	webgl.useProgram(shaderProgram);

	//Assign the vertex attribute position to the shader program
	shaderProgram.vertexPositionAttribute = webgl.getAttribLocation(shaderProgram, "aVertexPosition");
	webgl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
	
	if(isTexture)
	{
		shaderProgram.textureCoordAttribute = webgl.getAttribLocation(shaderProgram, "aTextureCoord");
		webgl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);
	}
	else
	{
		shaderProgram.vertexColorAttribute = webgl.getAttribLocation(shaderProgram, "aVertexColor");
		webgl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);
	}

	//Configure the uniforms
	shaderProgram.pMatrixUniform = webgl.getUniformLocation(shaderProgram, "uPMatrix");
	shaderProgram.mvMatrixUniform = webgl.getUniformLocation(shaderProgram, "uMVMatrix");
	
	if(isTexture)
	{
		shaderProgram.samplerUniform = webgl.getUniformLocation(shaderProgram, "uSampler");
		webgl.shaderProgramTextured = shaderProgram;
	}
	else
	{
		webgl.shaderProgram = shaderProgram;
	}
}

function setupScene()
{
	printCameraPosition();
	obj3Ds = [];

     var color = [0, 1, 0];
     
    var step = 100;
    for(var x = 0; x < canvas.width; x += step)
    {
    	for(var y = 0; y < canvas.height; y += step)
    	{
    		var width = step;
    		var height = step;
    		if(canvas.width - x < step)
    			width = canvas.width - x;
    		if(canvas.height - y < step)
    			height = canvas.height - y;
    		var overflowX = Math.round(x / step);
    		var overflowY = Math.round(y / step);
    		// if(x > 0)
    		// 	overflowX = 1;
    		// if(y > 0)
    		// 	overflowY = 1;
    		
    		obj3Ds.push(createPlane(1, color, [x - overflowX, 0, y - overflowY], [0, 0, 0], false, x - overflowX, y - overflowY, width, height));		
    	}
    }
    //obj3Ds.push(createPlane(1, color, [0, 0, 0], [0, 0, 0], false, 0, 0, 100, 100));		
    //obj3Ds.push(createPlane(1, color, [99, -1.0, 0], [0, 0, 0], false, 100, 0, 100, 100));
}

function printCameraPosition()
{
	document.getElementById("msg2").innerHTML = "pitch: " + camera.pitch.toFixed(2) + 
				" yaw: " + camera.yaw.toFixed(2) + " [" + camera.position[0].toFixed(0) + ", " +
				camera.position[1].toFixed(0) + ", " + camera.position[2].toFixed(0) + "]";
}

function createPlane(size, color, position, rotation, texture, offsetX, offsetY, width, height)
{
	var plane = {
    	obj: {},
    	picker: {},
    	sides: 1
    };
	var hs = size / 2;
	var hs2 = hs + border;
	
	var verts = [];
	var indices = [];
	var colors = [];
	var outline = [];
	var count = 0;
	for(var x = 0; x < width; x++)
	{
	    for(var y = 0; y < height; y++)
	    {
        	verts.push(x * size);
        	verts.push(map.data[x + offsetX][y + offsetY].height / 3);
        	verts.push(y * size);
        	
        	colors.push(map.data[x + offsetX][y + offsetY].color2.red / 256);
        	colors.push(map.data[x + offsetX][y + offsetY].color2.green / 256);
        	colors.push(map.data[x + offsetX][y + offsetY].color2.blue / 256);
        	colors.push(1);
        	
        	//colors.push((count % 4) / 4);
        	//colors.push((count % 4) / 4);
        	//colors.push((count % 4) / 4);
        	//colors.push(1);
        	count++;
        	if(x < width - 1 && y < height - 1)
        	{
        		var topIndex = (width * y) + x;
        		var bottomIndex = (width * (y + 1)) + x;
	        	indices.push(topIndex);
	        	indices.push(topIndex + 1);
	        	indices.push(bottomIndex + 1);
	        	
	        	indices.push(topIndex);
	        	indices.push(bottomIndex + 1);
	        	indices.push(bottomIndex);
        	}
	    }
	}
	
	var textureCoords = false;
	if(texture)
	{
		plane.obj.texture = texture;
		textureCoords = [ 
			 //Top
			 1.0, 0.0,
			 1.0, 1.0,
			 0.0, 0.0,
			 0.0, 1.0
		];
	}
	
	plane.id = maxId;
	maxId += plane.sides;
	
	create3dObj(webgl, plane.obj, plane.id, 0, verts, colors, indices, false, [0, 0, 0, 1], textureCoords);
	
	plane.position = position;
    plane.rotation = rotation;
    
	return plane;
}

function create3dObj(webgl, obj3D, id, sides, verts, colors, indices, outline, outlineColor, textureCoords)
{
	//Vertices
	obj3D.vbo = webgl.createBuffer();
    webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.vbo);
    webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(verts), 
        webgl.STATIC_DRAW);
    obj3D.vbo.itemSize = 3;
    obj3D.vbo.numItems = verts.length / 3;
   
    //Colors
    obj3D.cbo = webgl.createBuffer();
    webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.cbo);
    webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(colors), 
        webgl.STATIC_DRAW);
    obj3D.cbo.itemSize = 4;
    obj3D.cbo.numItems = colors.length / 4;
    
    //Indices
    obj3D.ibo = webgl.createBuffer();
    webgl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, obj3D.ibo);
    webgl.bufferData(webgl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), 
        webgl.STATIC_DRAW);
    obj3D.ibo.itemSize = 1;
    obj3D.ibo.numItems = indices.length;
    
    //Outline
    if(outline)
    {
	    obj3D.outvbo = webgl.createBuffer();
	    webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.outvbo);
	    webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(outline), 
	        webgl.STATIC_DRAW);
	    obj3D.outvbo.itemSize = 3;
	    obj3D.outvbo.numItems = outline.length / 3;
	    
	    var outlineColors = [];
	    for(var i = 0; i < obj3D.outvbo.numItems; i++)
	    {
	    	outlineColors = outlineColors.concat(outlineColor);
	    }
	    obj3D.outcbo = webgl.createBuffer();
	    webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.outcbo);
	    webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(outlineColors), 
	        webgl.STATIC_DRAW);
	    obj3D.outcbo.itemSize = 4;
	    obj3D.outcbo.numItems = outlineColors.length / 4;
    }
    
    if(obj3D.texture)
    {
    	obj3D.tbo = webgl.createBuffer();
    	webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.tbo);
    	webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(textureCoords), 
    		webgl.STATIC_DRAW);
    	obj3D.tbo.itemSize = 2;
    	obj3D.tbo.numItems = textureCoords.length / 2;
    }
}

function draw3d(webgl)
{
	var shaderProgram;
	if(webgl.isTextured)
	{
		shaderProgram = webgl.shaderProgramTextured;
	}
	else
	{
		shaderProgram = webgl.shaderProgram;
	}
	webgl.useProgram(shaderProgram);
	
    //Setup the viewport
	webgl.viewport(0, 0, webgl.viewportWidth, webgl.viewportHeight);
	
	//Clear the background
	webgl.clear(webgl.COLOR_BUFFER_BIT | webgl.DEPTH_BUFFER_BIT);
	
	//Setup the perspective matrix
	mat4.perspective(85, webgl.viewportWidth / webgl.viewportHeight, 0.1, 
	    1000.0, pMatrix);	
	webgl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
	
	var mvStart = mat4.create();
	mvMatrix = camera.GetMatrix();
	mat4.set(mvMatrix, mvStart);
	
	for(var i = 0; i < obj3Ds.length; i++)
	{
		//mat4.identity(mvMatrix);
		mat4.set(mvStart, mvMatrix);
	    drawObject(obj3Ds[i], obj3Ds[i].obj, webgl);
	}
}

function drawObject(obj, obj3D, webgl)
{
	var shaderProgram;
	if(obj3D.texture)
	{
		shaderProgram = webgl.shaderProgramTextured;
	}
	else
	{
		shaderProgram = webgl.shaderProgram;
	}
	webgl.useProgram(shaderProgram);
	
	mat4.translate(mvMatrix, obj.position);
	mat4.rotate(mvMatrix, vec3.length(obj.rotation), obj.rotation, 
		mvMatrix);

	//Copy the mvMatrix uniform to the shader program
	webgl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
	
	//Vertices
	webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.vbo);
	webgl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 
		obj3D.vbo.itemSize, webgl.FLOAT, false, 0, 0);
	
	if(obj3D.texture)
	{
		//Texture
		webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.tbo);
		webgl.vertexAttribPointer(shaderProgram.textureCoordAttribute, obj3D.tbo.itemSize, webgl.FLOAT, false, 0, 0);
		webgl.activeTexture(webgl.TEXTURE0);
		webgl.bindTexture(webgl.TEXTURE_2D, obj3D.texture);
		webgl.uniform1i(shaderProgram.samplerUniform, 0);
	}
	else
	{
		//Colors
		webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.cbo);
		webgl.vertexAttribPointer(shaderProgram.vertexColorAttribute, 
			obj3D.cbo.itemSize, webgl.FLOAT, false, 0, 0);
	}
	//Indices
	webgl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, obj3D.ibo);
	webgl.drawElements(webgl.TRIANGLES, obj3D.ibo.numItems, 
		webgl.UNSIGNED_SHORT, 0);
	
	//Draw the outline
	if(obj3D.outvbo)
	{
		webgl.useProgram(webgl.shaderProgram);
		webgl.uniformMatrix4fv(webgl.shaderProgram.pMatrixUniform, false, pMatrix);
		webgl.uniformMatrix4fv(webgl.shaderProgram.mvMatrixUniform, false, mvMatrix);
		
		webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.outvbo);
		webgl.vertexAttribPointer(webgl.shaderProgram.vertexPositionAttribute, 
			obj3D.outvbo.itemSize, webgl.FLOAT, false, 0, 0);
		webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.outcbo);
		webgl.vertexAttribPointer(webgl.shaderProgram.vertexColorAttribute, 
			obj3D.outcbo.itemSize, webgl.FLOAT, false, 0, 0);
		webgl.drawArrays(webgl.LINE_STRIP, 0, obj3D.outvbo.numItems);
	}
}

var msg2 = "";
function run()
{
	move();
	draw3d(webgl);
	
	calcFPS();
	window.requestAnimationFrame(run);
}

function move()
{
	var radius = 1;
	if(movement.raise)
	{
		camera.position[1] += radius / 2;
		camera.Update(0,0,0);
	}
	
	if(movement.lower)
	{
		camera.position[1] -= radius / 2;
		camera.Update(0,0,0);
	}
	
	if(movement.left)
	{
		//camera.yaw += 0.05;
		//camera.Update(0, 0, 0);
		camera.position[2] -= Math.cos(camera.yaw + Math.PI / 2) * radius;
		camera.position[0] -= Math.sin(camera.yaw + Math.PI / 2) * radius;
		camera.Update(0, 0, 0);		
	}
	
	if(movement.right)
	{
		//camera.yaw -= 0.05;
		//camera.Update(0, 0, 0);	
		camera.position[2] -= Math.cos(camera.yaw - Math.PI / 2) * radius;
		camera.position[0] -= Math.sin(camera.yaw - Math.PI / 2) * radius;
		camera.Update(0, 0, 0);	
	}
	
	if(movement.up)
	{
		camera.position[2] -= Math.cos(camera.yaw) * radius;
		camera.position[0] -= Math.sin(camera.yaw) * radius;
		camera.Update(0, 0, 0);		
	}
	
	if(movement.back)
	{
		camera.position[2] += Math.cos(camera.yaw) * radius;
		camera.position[0] += Math.sin(camera.yaw) * radius;
		camera.Update(0, 0, 0);		
	}
	
	printCameraPosition();
}

function calcFPS()
{
	var d = new Date();
	t = d.getTime();
	frames++;
	if(t - lt > 1000)
	{
		//fps = Math.round( (frames / ((t - lastTime) / 1000)) * 100) / 100;
		var fps = frames / ((t - lt) / 1000);
		document.getElementById("msg").innerHTML = "FPS: " + fps.toFixed(2);
		frames = 0;
		lt = t;
	}
	
	//document.getElementById("msg2").innerHTML = "t: " + (t - lastDrawTime).toFixed(0);
}
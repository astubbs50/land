"use strict";

var canvas;
var context;

function init()
{
	canvas = document.getElementById("myCanvas");
	context = canvas.getContext("2d");
	started = true;
	//updateSteps();
	updateScales();
	//draw();
}
var started = false;

var t = 0;
var scale = 0.0037;
var scale2 = 5882;
var scale3 = 23.23;
var scale4 = 7972;
var offset = 66;
var high;
var low;
var inverted = false;
var seed = 0;

function draw()
{
	//x2 += Math.random() * 20 - 10;
	//y2 += Math.random() * 20 - 10;
	
	//scale += Math.random() * 0.002 - 0.001;
	//scale2a += Math.random() * 0.0002 - 0.0001;
	
	//t += Math.random() * 0.2 - 0.1;
	//t++
	
	noise.seed(seed);
	var step = 5;
	
	
	var centerX = (canvas.width / 2);
	var centerY = (canvas.height / 2);
	
	low = 99999999;
	high = -99999999;
	
	for (var x = 0; x < canvas.width; x += step) 
	{
	  for (var y = 0; y < canvas.height; y += step) 
	  {
		var dx = x - centerX;
		var dy = y - centerY;
		var d = Math.sqrt(dx * dx + dy * dy);
		//d = 1;
		var value = (((noise.perlin2(x * scale, y * scale) * scale2 - (d * scale3))) + offset) / scale4;
		//var value2 = Math.random() * 0.5;
		//var value3 = ((noise.perlin3(x * scale, y * scale, t + 80) - d * scale2) + 13) / 13;
		//var value4 = Math.random() * 0.5;
		//var value5 = Math.random() * 0.5;
		
		if(value < low)
			low = value;
		if(value > high)
			high = value;
			
		//image[x][y].r = Math.abs(value) * 256; // Or whatever. Open demo.html to see it used with canvas.
		/*context.fillStyle = "rgb(" + 
			Math.floor(Math.abs(value) * 256) + ", " + 
			Math.floor(Math.abs(value) * 256) + ", " + 
			Math.floor(Math.abs(value) * 256) + ")";
		*/
		/*context.fillStyle = "rgba(" + 
			Math.floor(Math.abs(value2) * 256) + ", " + 
			Math.floor(Math.abs(value2) * 256) + ", " + 
			Math.floor(Math.abs(value2) * 256) + ", " +
			Math.floor(Math.abs(value * 1.1)) + ")";
		*/
		//context.fillStyle = "rgba(55, 55, 55, " + Math.abs(value) + ")";
		//if(value < 0)
		//	value = 0;
		//if(value > 1)
		//	value = 1;
		
		//if(value > -1500)
		//	value = 1;
		//else
		//	value = 0;
		
		//value -= 0.3;
	//	if(value > 0.3)
	//		value = 1;
	//	else
	//		value = 0;
	//		
		// context.fillStyle = "rgb(" + 
		// 	Math.floor(Math.min(255 - Math.abs(value) * 256, 255)) + ", " + 
		// 	Math.floor(Math.min(255 - Math.abs(value) * 256, 255)) + ", " + 
		// 	Math.floor(Math.min(255 - Math.abs(value) * 256, 255)) + ")";
			
		if(inverted)
		{
			context.fillStyle = "rgb(" + 
				Math.floor(255 - Math.abs(value) * 256) + ", " + 
				Math.floor(255 - Math.abs(value) * 256) + ", " + 
				Math.floor(255 - Math.abs(value) * 256) + ")";
		}	
		else
		{
			context.fillStyle = "rgb(" + 
				Math.floor(Math.abs(value) * 256) + ", " + 
				Math.floor(Math.abs(value) * 256) + ", " + 
				Math.floor(Math.abs(value) * 256) + ")";
		}
		context.fillRect(x, y, step, step);
	  }
	}
//	alert("hi: " + high + " low: " + low);
	
	//requestAnimationFrame(draw);
}

function updateScales()
{
	if(started)
	{
		seed = document.getElementById("seed").value;
		document.getElementById("spanSeed").innerHTML = seed;
		
		scale = document.getElementById("scale").value;
		document.getElementById("spanScale").innerHTML = scale;
		
		scale2 = document.getElementById("scale2").value;
		document.getElementById("spanScale2").innerHTML = scale2;
		
		scale3 = document.getElementById("scale3").value;
		document.getElementById("spanScale3").innerHTML = scale3;
		
		scale4 = document.getElementById("scale4").value;
		document.getElementById("spanScale4").innerHTML = scale4;
		
		offset = document.getElementById("offset").value;
		document.getElementById("spanOffset").innerHTML = offset;
		
		inverted = document.getElementById("inverted").checked;
		
		//alert(scale);
		draw();
	}
}

function randomize()
{
	seed = Math.round(Math.random() * (document.getElementById("seed").max - document.getElementById("seed").min) + document.getElementById("seed").min);
	document.getElementById("spanSeed").innerHTML = seed;
	
	scale = (Math.random() * (document.getElementById("scale").max - document.getElementById("scale").min) + document.getElementById("scale").min);
	document.getElementById("spanScale").innerHTML = scale;
	
	scale2 = (Math.random() * (document.getElementById("scale2").max - document.getElementById("scale2").min) + document.getElementById("scale2").min);
	document.getElementById("spanScale2").innerHTML = scale2;
	
	scale3 = (Math.random() * (document.getElementById("scale3").max - document.getElementById("scale3").min) + document.getElementById("scale3").min);
	document.getElementById("spanScale3").innerHTML = scale3;
	
	scale4 = Math.round(Math.random() * (document.getElementById("scale4").max - document.getElementById("scale4").min) + document.getElementById("scale4").min);
	document.getElementById("spanScale4").innerHTML = scale4;
	
	offset = Math.round(Math.random() * (document.getElementById("seed").max - document.getElementById("seed").min) + document.getElementById("seed").min);
	document.getElementById("spanOffset").innerHTML = offset;
	
	draw();
}

function updateCheckboxes()
{
	setTimeout(updateScales, 50);
}

function autoScale()
{
	scale4 = 1;
	offset = 0;
	draw();
	scale4 = Math.round(Math.abs(high) + Math.abs(low));
	offset = scale4;
	document.getElementById("spanScale4").innerHTML = scale4;
//	if(document.getElementById("scale4").max < scale4)
//		document.getElementById("scale4").max = scale4 + 1;
	document.getElementById("scale4").value = scale4;
	
	
//	if(document.getElementById("offset").max < offset)
//		document.getElementById("offset").max = offset;
	document.getElementById("spanOffset").innerHTML = offset;	
	document.getElementById("offset").value = offset;
	draw();
}

function updateSteps()
{
	var step = document.getElementById("scaleStep").value;
	document.getElementById("spanScaleStep").innerHTML = step;
	document.getElementById("scale").step = step;
}

function keyDown(e)
{		
	setAction(e.keyCode, true);
}

function keyUp(e)
{
	//document.getElementById("stats").innerHTML = e.keyCode;
	setAction(e.keyCode, false);
}

function setAction(keyCode, bOn)
{		
	switch(keyCode)
	{
		//Left
		case 65:
		case 37:
		case 100:
			//player.moveLeft = bOn;	
			//pacman.moveRight = false;
			break;
		//Right
		case 68:
		case 39:
		case 102:
			//player.moveRight = bOn;			
			//pacman.moveLeft = false;
			break;
		//Up
		case 87:
		case 38:
		case 104:
			//player.jump = bOn;	
			//.moveDown = false;
			break;	
		//Down
		case 40:
		case 83:
			//player.moveDown = bOn;			
			//pacman.moveUp = false;
			break;			
	}
}
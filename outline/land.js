"use strict";

var canvas;
var context;

function init()
{
	canvas = document.getElementById("myCanvas");
	context = canvas.getContext("2d");
	started = true;

	createMap(true);
	getOutline();
}
var started = false;

var t = 0;
var seed = 0;

function createMap(outline)
{
	var scale = 0.0057;
	var scale2 = 5882;
	var scale3 = 28.23;
	var scale4 = 7972;
	var offset = 66;
	var high;
	var low;
	var inverted = true;
	var seed = 0;
	var step = 1;
	
	//noise.seed(seed);
	noise.seed(seed);
	
	var centerX = (canvas.width / 2);
	var centerY = (canvas.height / 2);
	
	low = 99999999;
	high = -99999999;
	
	var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
	var data = imageData.data;
	
	for (var x = 0; x < canvas.width; x += step) 
	{
	  for (var y = 0; y < canvas.height; y += step) 
	  {
		var dx = x - centerX;
		var dy = y - centerY;
		var d = Math.sqrt(dx * dx + dy * dy);

		var value = (((noise.perlin2(x * scale, y * scale) * scale2 - (d * scale3))) + offset) / scale4;

		if(value < low)
			low = value;
		if(value > high)
			high = value;
	
		var index = ((canvas.width * y) + x) * 4;
		
		 var colorValue = (255 - Math.abs(value) * 256);
		 	
		 colorValue = Math.min(colorValue, 255);
		 colorValue = Math.max(colorValue, 0);
		 
		 if(outline && colorValue > 0)
		 	colorValue = 255;
		 	
		 data[index] = colorValue;
         data[index + 1] = colorValue;
         data[index + 2] = colorValue;
         data[index + 3] = 255;
	  }
	}
	context.putImageData(imageData, 0, 0);
	//alert("hi: " + high + " low: " + low);
}

function getOutline()
{
	var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
	var data = imageData.data;
	var outline = [];
	
	var search = [];
	search.push({
		x: Math.floor(canvas.width / 2),
		y: Math.floor(canvas.height / 2)
	});
	
	while(search.length > 0)
	{
		var pixel = search.shift();
		var index = ((canvas.width * pixel.y) + pixel.x) * 4;
		
		var value = data[index];
		var searched = data[index + 1];
		
		if(value === 0 && searched === 0)
		{
			outline.push(pixel);
		}
		else if(searched !== 1)
		{
			search.push({
				x: pixel.x + 1,
				y: pixel.y
			});
			search.push({
				x: pixel.x - 1,
				y: pixel.y
			});
			search.push({
				x: pixel.x,
				y: pixel.y + 1
			});
			search.push({
				x: pixel.x,
				y: pixel.y - 1
			});
		}
		
		data[index + 0] = 0;
		data[index + 1] = 1;
		data[index + 2] = 0;
	}
	
	// for(var i = 0; i < outline.length; i++)
	// {
	// 	var pixel = outline[i];
	// 	// var pixel2 = {
	// 	// 	x: pixel.x + Math.floor(Math.random() * 30 - 15),
	// 	// 	y: pixel.y + Math.floor(Math.random() * 30 - 15)
	// 	// };
	// 	// var index = ((canvas.width * pixel2.y) + pixel2.x) * 4;
	// 	// data[index + 1] = 255;
	// 	roughEdges(pixel.x, pixel.y, data);
	// }
	
	//context.putImageData(imageData, 0, 0);
	roughEdges2(outline);
	//alert(outline.length);
}

function roughEdges2(outline)
{
	console.log("roughEdges2");
	var outline2 = [];
	var index = 0;
	while(outline.length > 0)
	{
		var pixel = outline[index];
		outline.splice(index, 1);
		outline2.push(pixel);
		
		var dMin = 999999999;
		
		for(var j = 0; j < outline.length; j++)
		{
			var dx = pixel.x - outline[j].x;
			var dy = pixel.y - outline[j].y;
			var d = dx * dx + dy * dy;
			if(d < dMin)
			{
				dMin = d;
				index = j;
			}
		}
	}
	
	outline = outline2;
	
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.beginPath();
	context.moveTo(outline[0].x, outline[0].y);
	
	var step = 5;
	var variance = 5;
	var halfVariance = variance / 2;
	console.log("draw outline");
	for(var i = step; i < outline.length - step; i += step)
	{
		pixel = outline[i];
		context.lineTo(
			pixel.x + Math.floor(Math.random() * variance) - halfVariance, 
			outline[i].y + Math.floor(Math.random() * variance) - halfVariance
		);
	}
	context.lineTo(outline[0].x, outline[0].y);
	
	context.strokeStyle = "rgb(255, 255, 255)";
	context.stroke();
}

function roughEdges(x, y, data)
{
	var startVal = 25;
	
	var pixels = [];
	var pixel = {
		x: x, 
		y: y, 
		val: startVal
	};
	pixels.push(pixel);
	var count = 0;
	while(pixels.length > 0 && count++ < 25)
	{
		pixel = pixels.pop();
		var index = ((canvas.width * pixel.y) + pixel.x) * 4;
		
		if(data[index + 1] !== 255 && Math.random() * startVal < pixel.val)
		{
			data[index + 0] = 0;
			data[index + 1] = 255;
			data[index + 2] = 0;
		
			pixels.push({
				x: pixel.x + 1,
				y: pixel.y,
				val: pixel.val - 1
			});
			pixels.push({
				x: pixel.x - 1,
				y: pixel.y,
				val: pixel.val - 1
			});
			pixels.push({
				x: pixel.x,
				y: pixel.y + 1,
				val: pixel.val - 1
			});
			pixels.push({
				x: pixel.x,
				y: pixel.y - 1,
				val: pixel.val - 1
			});
		}
	}
}

function draw()
{
	//x2 += Math.random() * 20 - 10;
	//y2 += Math.random() * 20 - 10;
	
	//scale += Math.random() * 0.002 - 0.001;
	//scale2a += Math.random() * 0.0002 - 0.0001;
	
	//t += Math.random() * 0.2 - 0.1;
	//t++
	
	noise.seed(seed);
	var step = 5;
	
	
	var centerX = (canvas.width / 2);
	var centerY = (canvas.height / 2);
	
	low = 99999999;
	high = -99999999;
	
	for (var x = 0; x < canvas.width; x += step) 
	{
	  for (var y = 0; y < canvas.height; y += step) 
	  {
		var dx = x - centerX;
		var dy = y - centerY;
		var d = Math.sqrt(dx * dx + dy * dy);
		//d = 1;
		var value = (((noise.perlin2(x * scale, y * scale) * scale2 - (d * scale3))) + offset) / scale4;
		//var value2 = Math.random() * 0.5;
		//var value3 = ((noise.perlin3(x * scale, y * scale, t + 80) - d * scale2) + 13) / 13;
		//var value4 = Math.random() * 0.5;
		//var value5 = Math.random() * 0.5;
		
		if(value < low)
			low = value;
		if(value > high)
			high = value;
			
		//image[x][y].r = Math.abs(value) * 256; // Or whatever. Open demo.html to see it used with canvas.
		/*context.fillStyle = "rgb(" + 
			Math.floor(Math.abs(value) * 256) + ", " + 
			Math.floor(Math.abs(value) * 256) + ", " + 
			Math.floor(Math.abs(value) * 256) + ")";
		*/
		/*context.fillStyle = "rgba(" + 
			Math.floor(Math.abs(value2) * 256) + ", " + 
			Math.floor(Math.abs(value2) * 256) + ", " + 
			Math.floor(Math.abs(value2) * 256) + ", " +
			Math.floor(Math.abs(value * 1.1)) + ")";
		*/
		//context.fillStyle = "rgba(55, 55, 55, " + Math.abs(value) + ")";
		//if(value < 0)
		//	value = 0;
		//if(value > 1)
		//	value = 1;
		
		//if(value > -1500)
		//	value = 1;
		//else
		//	value = 0;
		
		//value -= 0.3;
	//	if(value > 0.3)
	//		value = 1;
	//	else
	//		value = 0;
	//		
		// context.fillStyle = "rgb(" + 
		// 	Math.floor(Math.min(255 - Math.abs(value) * 256, 255)) + ", " + 
		// 	Math.floor(Math.min(255 - Math.abs(value) * 256, 255)) + ", " + 
		// 	Math.floor(Math.min(255 - Math.abs(value) * 256, 255)) + ")";
			
		if(inverted)
		{
			context.fillStyle = "rgb(" + 
				Math.floor(255 - Math.abs(value) * 256) + ", " + 
				Math.floor(255 - Math.abs(value) * 256) + ", " + 
				Math.floor(255 - Math.abs(value) * 256) + ")";
		}	
		else
		{
			context.fillStyle = "rgb(" + 
				Math.floor(Math.abs(value) * 256) + ", " + 
				Math.floor(Math.abs(value) * 256) + ", " + 
				Math.floor(Math.abs(value) * 256) + ")";
		}
		context.fillRect(x, y, step, step);
	  }
	}
//	alert("hi: " + high + " low: " + low);
	
	//requestAnimationFrame(draw);
}

function keyDown(e)
{		
	setAction(e.keyCode, true);
}

function keyUp(e)
{
	//document.getElementById("stats").innerHTML = e.keyCode;
	setAction(e.keyCode, false);
}

function setAction(keyCode, bOn)
{		
	switch(keyCode)
	{
		//Left
		case 65:
		case 37:
		case 100:
			//player.moveLeft = bOn;	
			//pacman.moveRight = false;
			break;
		//Right
		case 68:
		case 39:
		case 102:
			//player.moveRight = bOn;			
			//pacman.moveLeft = false;
			break;
		//Up
		case 87:
		case 38:
		case 104:
			//player.jump = bOn;	
			//.moveDown = false;
			break;	
		//Down
		case 40:
		case 83:
			//player.moveDown = bOn;			
			//pacman.moveUp = false;
			break;			
	}
}